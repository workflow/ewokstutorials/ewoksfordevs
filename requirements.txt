# Notebook
jupyterlab
jupyterlab-spellchecker

# Style
black[jupyter]
flake8_nb

# Notebook Content
ewoks[ppf]