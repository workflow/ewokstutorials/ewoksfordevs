# Contributing

## Installation

```
pip install -r requirements.txt
```

## See the slideshow

```
jupyter nbconvert ewoksfordevs.ipynb --embed-images --to slides --post serve
```

## Create static slideshow (HTML, PDF)

```
jupyter nbconvert ewoksfordevs.ipynb --embed-images --to slides
jupyter nbconvert ewoksfordevs.ipynb --embed-images --to pdf
```

## Generate all notebook results

```
jupyter nbconvert ewoksfordevs.ipynb --to notebook --execute
```

## Style checks

```
black .
flake8
flake8_nb
```

## ReadTheDocs

Documentation is automatically uploaded to https://ewoksfordevs.readthedocs.io/ on updates of the `main` branch.

More specifically:
- A push on `main` triggers the update of the `latest` version.
- A tag on `main` triggers the update of the `stable` version.
