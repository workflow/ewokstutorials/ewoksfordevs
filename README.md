# Ewoks for developers

## Quick start

Download all tutorial resources

```
git clone https://gitlab.esrf.fr/workflow/ewokstutorials/ewoksfordevs.git
```

The slideshow can be found [here](https://workflow.gitlab-pages.esrf.fr/ewokstutorials/ewoksfordevs/) or
as PDF ([slides](https://workflow.gitlab-pages.esrf.fr/ewokstutorials/ewoksfordevs/ewoksfordevs.slides.pdf),
[book](https://workflow.gitlab-pages.esrf.fr/ewokstutorials/ewoksfordevs/ewoksfordevs.pdf)).

## Training instructions

Additional instructions can be found [here](https://gitlab.esrf.fr/workflow/ewokstutorials/ewoksfordevs/-/wikis/pages)

## Program

0. Introduction and program _(15mn)_
1. Ewoks workflows _(2h30)_
   - Make a workflow and execute it
   - Execute a workflow with different engines
   - Execute a workflow by specifying inputs/outputs
   - Use the web GUI to create, save and execute workflows
2. Ewoks tasks _(1h30)_
   - Create ewoks tasks and use them in a workflow
   - Use new tasks in the GUI
   - Create a workflow from existing tasks in the GUI
3. Ewoks integration _(2h)_
   - Setup and test remote execution
   - Submit a workflow to a remote worker
   - Submit a workflow to Slurm
   - Setup workflows with Bliss to process a live XRPD scan
4. Final remarks _(15mn)_
